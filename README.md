# README #

A puppet module to carry out a minimal install of Lighttpd on RHEL/CENTOS

## Usage ##

This module installs Lighttpd. It currently has very few configurable options, other than setting the server root and adding one or more fastcgi configurations


### Minimal setup ###
The example below shows the syntax for installing lighttpd, configuring the server and document roots, and setting up two fcgi endpoints. Additional resources would be required in practice, e.g. iipsrv binary etc.

See staging and production site.pp for full server examples.

```
#!puppet

node 'mynode' {
  # Install Lighttpd
  class {'lighttpd':
    server_root      => '/var/www',
    document_root    => '/var/www/mynode'
  }

  # Setup a fastcgi instance for iipsrv
  lighttpd::fastcgi{'iipsrv':
     extension => '/fcgi-bin/iipsrv.fcgi',
     config => {
       "socket" => "/var/run/lighttpd/iipsrv-fastcgi.socket",
       "check-local" => "disable",
       "min-procs" => 1,
       "max-procs" => 8,
       "bin-path" => "/usr/local/iipsrv/iipsrv.fcgi",
       "bin-environment" => {
         "LOGFILE" => "/tmp/lighttpd-iipsrv.log",
         "VERBOSITY" => "5",
         "MAX_IMAGE_CACHE_SIZE" => "10",
         "JPEG_QUALITY" => "75",
         "FILESYSTEM_PREFIX" => "/tmp/",
      },
    },
  }

  # Setup a fastcgi instance for divaserve
  lighttpd::fastcgi{'php':
     extension => '.php',
     config => {
       "host" => "127.0.0.1",
       "port" => '9000',
       "check-local" => "disable",
       "broken-scriptfilename" => "enable",
    },
  }

  include ::php

}
```
