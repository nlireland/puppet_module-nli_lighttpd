define lighttpd::module (
  $include_file = undef
) {
  include 'lighttpd'

  file_line { "mod_$title":
    line => "server.modules += (\"mod_$title\")",
    path => '/etc/lighttpd/modules.conf',
    notify  => Service['lighttpd']
  }

  if $include_file !=undef {
    file_line { "include_$title":
      line => "include \"conf.d/$include_file\"",
      path => '/etc/lighttpd/modules.conf',
      notify  => Service['lighttpd']
    }
  }

}
