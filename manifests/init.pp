class lighttpd (
  $server_root             = $lighttpd::params::server_root,
  $document_root           = $lighttpd::params::document_root,
  $server_max_fds          = $lighttpd::params::server_max_fds,
  $max_connections         = $lighttpd::params::max_connections,
  $max_keep_alive_requests = $lighttpd::params::max_keep_alive_requests,
) inherits lighttpd::params {

  include 'epel'

  package {'lighttpd':
   ensure => installed,
   require => Class['epel']
  }

  package {'lighttpd-fastcgi':
   ensure => installed,
   require => Package['lighttpd']
  }

  service {'lighttpd':
   ensure => running,
   enable => true,
   require => Package['lighttpd']
  }

  augeas {"lighhtpd-conf":
   lens => "Properties.lns",
   incl => "/etc/lighttpd/lighttpd.conf",
   changes => [
     "set var.server_root '\"$server_root\"'",
     "set server.document-root '\"$document_root\"'",
     "set server.max-fds '$server_max_fds'",
     "set server.max-connections '$max_connections'",
     "set server.max-keep-alive-requests '$max_keep_alive_requests'",
     "set server.bind '\"0.0.0.0\"'",
   ],
   require => Package['lighttpd'],
   notify => Service['lighttpd'],
  }
}
