define lighttpd::fastcgi (
  $extension          = '.php',
  $config             = {}
) {
  include 'lighttpd'
  $file_name = "$title.conf"

  file {"/etc/lighttpd/conf.d/$file_name":
    ensure  => file,
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    content => template('lighttpd/fastcgi_template.erb'),
    require => Package['lighttpd'],
    notify  => Service['lighttpd']
  }

  file_line { "include_$title":
    line => "include \"conf.d/$file_name\"",
    path => '/etc/lighttpd/lighttpd.conf',
    require => File["/etc/lighttpd/conf.d/$file_name"],
    notify  => Service['lighttpd']
  }

}
