class lighttpd::params {
  $server_root             = '/var/www'
  $document_root           = '/var/www/lighttpd'
  $server_max_fds          = '1024'
  $max_connections         = '1024'
  $max_keep_alive_requests = '16'
}
